<?php
/**
 * Created by PhpStorm.
 * User: kseniya
 * Date: 18.06.18
 * Time: 16:23
 */

namespace App\Model\Client;

use App\Entity\Client;

class ClientHandler
{
    /**
     * @param array $data
     * @return Client
     */
    public function createNewClient(array $data) {
        $client = new Client();
        $client->setEmail($data['email']);
        $client->setPassport($data['passport']);
        $password = md5($data['password']).md5($data['password'].'2');
        $client->setPassword($password);

        return $client;
    }
}
