<?php
namespace App\DataFixtures;

use App\Entity\Client;
use App\Entity\User;
use App\Model\Client\ClientHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ClientFixtures extends Fixture
{

    /**
     * @var ClientHandler
     */
    private $clientHandler;

    public function __construct(ClientHandler $clientHandler)
    {
        $this->clientHandler = $clientHandler;
    }

    public function load(ObjectManager $manager)
    {
        $client = $this->clientHandler->createNewClient([
            'email' => 'kkk@kkk.ru',
            'passport' => 'passport 111',
            'password' => 'pass_1234',
        ]);

        $manager->persist($client);

        $client = $this->clientHandler->createNewClient([
            'email' => 'lll@lll.ru',
            'passport' => 'passport 222',
            'password' => 'pass_5678',
        ]);

        $manager->persist($client);
        $manager->flush();
    }
}